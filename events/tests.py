# Create your tests here.
from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest
from pendaftaran.models import EventRegis

# Create your tests here.

class EventsPageTest(TestCase):

    def test_home_url_exist(self):
        #test url '/' exist
        event = EventRegis.objects.create(
            location = "jkt",
            corporate_name = "name",
            address = "add",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "lalala")
        response = self.client.get('/events/1')
        self.assertEqual(response.status_code, 200)

    def test_home_calling_events_views_function(self):
        event = EventRegis.objects.create(
            location = "jkt",
            corporate_name = "name",
            address = "add",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "lalala")
        found = resolve('/events/1')
        self.assertEqual(found.func, views.events)

    def test_home_using_events(self):
        #test url '/' will using landing page template
        event = EventRegis.objects.create(
            location = "jkt",
            corporate_name = "name",
            address = "add",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "lalala")
        response = self.client.get('/events/1')
        self.assertTemplateUsed(response, 'gallery.html')

    def test_events_view(self):
        event = EventRegis.objects.create(
            location = "jkt",
            corporate_name = "name",
            address = "add",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "lalala")
        request = HttpRequest()
        response = views.events(request,1)
        self.assertContains(response, '')

class ParticipantTest(TestCase):
    def test_participant_url_exist(self):
        response = self.client.get('/events/registrasi/')
        self.assertEqual(response.status_code, 200)

    def test_participant_views_function(self):
        found = resolve('/events/registrasi/')
        self.assertEqual(found.func, views.Join)
    
    def test_participant_view(self):
        request = HttpRequest()
        response = views.Join(request)
        self.assertContains(response, '')

