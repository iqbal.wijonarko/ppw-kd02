from django.shortcuts import render, redirect
from .forms import contactForm
from .models import Contact
from .forms import contactForm

# Create your views here.
def createContact(request):
    form_contact = contactForm(request.POST or None)

    if request.method =='POST':
        if form_contact.is_valid():
            form_contact.save()

            return redirect('home')
        
    context = {
        'form_contact' : form_contact
    }

    return render(request,'contact.html',context)