from django.db import models

# Create your models here.
class Contact(models.Model):
    name         = models.CharField(max_length = 50)
    email        = models.CharField(max_length = 150)
    location     = models.CharField(max_length = 50)
    message      = models.TextField() 
    LIST_ABOUT = (
        ('Form Registasi', 'Form Registrasi'),
        ('Customer Service', 'Customer Service')
    )

    about = models.CharField(
        max_length = 50,
        choices = LIST_ABOUT,
    )

    def __str__(self):
         return "{}.{}".format(self.id,self.message)