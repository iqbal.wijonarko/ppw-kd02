# Tugas Kelompok 1 PPW-KD02

Kelompok KD02

Nama Anggota:

Leonardo (1806191023)

Muhammad Iqbal Wijonarko (1806186736)

Raul Arrafi Delfarra (1806205142)

Cindy S Evik (1806190903)

Sabila Nurwardani (1806147155)

Dalam Tugas kelompok ppw pertama, kami berencana untuk membuat web tentang 
pencarian event dan terinspirasi dari jobfair. Website kami lebih mengarah 
kepada tema technical assistance di era industry 4.0. Tujuan dari dibuatnya 
website kami untuk memudahkan di semua kalangan khususnya pelajar dalam mencari 
atau mendaftar suatu event yang bermanfaat. Kami sudah berencana membagi 
beberaapa fitur yang sudah kami tentukan pembagian untuk developnya yaitu :

-	Galerry event dan comment mengenai rincian event (Iqbal)
-	Sign in/ Registrasi sebagai peserta di suatu event (sabila)
-	Gallery keseluruhan dan fitur feedback di web kami / homepage (Leo)
-	Form pendaftaran peserta (Raul)
-	Form untuk pengajuan pendafaran event (Cindy)

Status Pipelines: Success
