from django import forms
from . import models


class CreateComment(forms.ModelForm):
    class Meta:
        model = models.Event
        fields = ['comment']

class ParticipantRegistration(forms.Form):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    Email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
    }))

    High_School_Student = forms.BooleanField(required=False)

    Undergraduate_Student = forms.BooleanField(required=False)

    Fresh_Graduate = forms.BooleanField(required=False)

    Other = forms.BooleanField(required=False)

    Company = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
    }))

    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
    }))

    Gender = forms.ChoiceField(choices = GENDER_CHOICES, required=True)

    Birthdate = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))

    Contact = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
    }))

    Privacy = forms.BooleanField(required=True)


