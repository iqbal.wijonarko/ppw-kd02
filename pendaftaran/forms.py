from django import forms
from .models import EventRegis
from django.forms import ModelForm

class EventForm (ModelForm):
    class Meta:
        model = EventRegis
        fields = ['location', 'corporate_name', 'address','tanggal','waktu', 'email', 'event', 'image', 'desc']
        labels = {
            'location' : 'Location', 'corporate_name' : 'Corporate Name', 'address' : 'Address', 'tanggal': 'Tanggal', 'waktu': 'Waktu', 'email' : 'Email', 'event' : 'Event', 'image' : 'Image', 'desc' : ''}
        widgets = {
            'location' : forms.TextInput(
                attrs = {'class': 'form-control',
                        'type' : 'text',
                        'placeholder' : 'Location'}),
            'corporate_name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Corporate Name'}),
            'address' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Address'}),
            'tanggal' : forms.DateInput(attrs={"class" : "form-control",
                                        'placeholder' : 'Tanggal (mm/dd/yyyy)'}),
            'waktu' : forms.TimeInput(attrs={
                                        "class" : "form-control",
                                        "placeholder":"Waktu (00:00)",
                                        }),
            'event' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Event'}),
            'email' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Email'}),
            'image' : forms.FileInput(),
            'desc' : forms.Textarea(attrs={'class': 'form-control',
                                        'placeholder' : 'Event Description'})
        }