# Generated by Django 2.2.6 on 2019-10-19 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20191019_1953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='Fresh_Graduate',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='participant',
            name='High_School_Student',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='participant',
            name='Other',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='participant',
            name='Undergraduate_Student',
            field=models.NullBooleanField(default=False),
        ),
    ]
