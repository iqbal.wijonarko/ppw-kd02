from django.db import models
from django.utils import timezone

# Create your models here.
class Feedback(models.Model):
	feedback_message = models.TextField(max_length=200)