from django import forms

from .models import Contact

class contactForm(forms.ModelForm):
    class Meta :
        model   = Contact
        fields  = [
            'name',
            'email',
            'location',
            'about',
            'message',
        ]

        widgets = {
            'name' : forms.TextInput(
                attrs={
                    'class' : 'form-control',

                }
            ),

            'email' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' :'@gmail.com'
                }

            ),

            'location' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                }

            ),

            'about' : forms.Select(
                attrs={
                    'class' : 'form-control',
                }
            ),

            'message' : forms.Textarea(
                attrs={
                    'class' : 'form-control',
                }
            ),

        }