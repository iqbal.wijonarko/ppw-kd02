from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Event(models.Model):
    comment = models.TextField(blank = True, null = True, max_length = 500)
    author = models.ForeignKey(User, default=None, on_delete=models.PROTECT)
    
    def __str__(self):
    	return self.comment 	

class Participant(models.Model):
	GENDER_CHOICES = (
	    ('M', 'Male'),
		('F', 'Female'),
    )
	Email = models.CharField(max_length=30)
	High_School_Student = models.BooleanField(default=False)
	Undergraduate_Student = models.BooleanField(default=False)
	Fresh_Graduate = models.BooleanField(default=False)
	Other = models.BooleanField(default=False)
	Company = models.CharField(max_length=30)
	Name = models.CharField(max_length=50)
	Gender = models.CharField(max_length=30, default='M', choices=GENDER_CHOICES)
	Birthdate = models.DateField(blank=False)
	Contact = models.CharField(max_length=30)
	Privacy = models.BooleanField(default=False)

	def __str__(self):
		return self.Name