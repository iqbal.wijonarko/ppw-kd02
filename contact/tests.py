from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest

# Create your tests here.

class ContactPageTest(TestCase):
    def test_contact_url_exist(self):
        #test url  '/' exist
        response = self.client.get('/contact/contact/')
        self.assertEqual(response.status_code, 200)
    
    def test_contact_using_contact_page(self):
        #test url '/' will using landing page template
        response = self.client.get('/contact/contact/')
        self.assertTemplateUsed(response, 'contact.html')
    
    def test_contact_calling_contact_views_function(self):
        found = resolve('/contact/contact/')
        self.assertEqual(found.func, views.createContact)
    
    

