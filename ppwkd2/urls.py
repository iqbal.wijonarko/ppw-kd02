"""ppwkd2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from landingpage.views import landing
from contact.views import createContact
from django.conf.urls.static import  static
from . import settings

from landingpage.views import landing
from contact.views import createContact
from django.conf.urls.static import  static
from . import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', landing, name="home"),
    path('contact/', include("contact.urls"), name='contact'),
    path('event-regis/', include('pendaftaran.urls'), name ="pendaftaran"),
    path('events/', include('events.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
