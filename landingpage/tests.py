from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest

# Create your tests here.

class LandingPageTest(TestCase):

    def test_home_url_exist(self):
        #test url '/' exist
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_calling_landing_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.landing)

    def test_home_using_landing_page(self):
        #test url '/' will using landing page template
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_landing_view(self):
        request = HttpRequest()
        response = views.landing(request)
        self.assertContains(response, '')
