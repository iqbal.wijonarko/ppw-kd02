
from django.urls import path
from .views import createContact

appname='contact'
urlpatterns = [
    path('contact/', createContact, name='contact'),
]