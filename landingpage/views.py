from django.shortcuts import render,redirect
from pendaftaran.models import EventRegis
from .forms import FeedbackForm

def landing(request):
    events = EventRegis.objects.all()
    form = FeedbackForm()
    if request.method == "POST":
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('home')
    return render(request, 'landing.html', {'events': events, 'form':form})