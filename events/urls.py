from django.urls import path
from . import views


app_name = 'events'

urlpatterns = [
    path('<int:id>', views.events, name='gallery'),
    path('registrasi/', views.Join, name='registrasi'),
]