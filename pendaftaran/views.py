from django.shortcuts import render
from .forms import EventForm
from .models import EventRegis
from django.shortcuts import redirect

def event_form(request):
	pendaftaran_form = EventForm()
	if request.method == "POST" :
		pendaftaran_form = EventForm(request.POST, request.FILES)
		if pendaftaran_form.is_valid():
			print (request.POST)
			pendaftaran_form.save()
			return redirect('pendaftaran')

	context =  {
		'form' : pendaftaran_form
	}
	return render(request, 'event_form.html', context)

# Create your views here.
