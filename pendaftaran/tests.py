from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest

# Create your tests here.

class EventRegisTest(TestCase):
    def tes_eventregis_url_exist(self):
        response = self.client.get('/event-regis/')
        self.assertEqual(response.status_code, 200)

    def test_eventregis_views_function(self):
        found = resolve('/event-regis/')
        self.assertEqual(found.func, views.event_form)
    
    def test_eventregis_view(self):
        request = HttpRequest()
        response = views.event_form(request)
        self.assertContains(response, '')
