from django.shortcuts import render, redirect
from . import forms
from pendaftaran.models import EventRegis
from .models import Participant as peserta
from .forms import ParticipantRegistration

def events(request,id):
    events = EventRegis.objects.get(id=id)
    if request.method == 'POST':
        form = forms.CreateComment(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            # form = forms.CreateComment()
            print(form)
            return redirect('events:gallery')
    else:
        form = forms.CreateComment()
    return render(request, 'gallery.html', {'form': form, 'event': events})
    
def Join(request):
    if request.method == "POST":
        form = ParticipantRegistration(request.POST)
        if form.is_valid():
            p = peserta()
            p.Email = form.cleaned_data['Email']
            p.High_School_Student = form.cleaned_data['High_School_Student']
            p.Undergraduate_Student = form.cleaned_data['Undergraduate_Student']
            p.Fresh_Graduate = form.cleaned_data['Fresh_Graduate']
            p.Other = form.cleaned_data['Other']
            p.Company = form.cleaned_data['Company']
            p.Name = form.cleaned_data['Name']
            p.Gender = form.cleaned_data['Gender']
            p.Birthdate = form.cleaned_data['Birthdate']
            p.Contact = form.cleaned_data['Contact']    
            p.Privacy = form.cleaned_data['Privacy']        
            p.save()
        return redirect('/')
    else:
        p = peserta.objects.all()
        form = ParticipantRegistration()
        response = {"p":p, 'form' : form}
        return render(request,'registrasi.html',response)




    